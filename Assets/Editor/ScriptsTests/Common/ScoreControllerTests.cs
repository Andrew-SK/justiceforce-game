﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using UnityEngine;

namespace JusticeForceTests
{
	[TestFixture]
	[Category("Score Controller Tests")]
	internal class ScoreControllerTests
	{
		ScoreController scoreController = new ScoreController();
		
		[Test]
		[Category("It should increment the score by a specified amount")]
		public void itShouldIncrementScoreByASpecifiedAmount()
		{
			scoreController.score = 30;

			scoreController.AlterScore(20);

			Assert.That (scoreController.score == 50);
		}

		[Test]
		[Category("It should not increment the score if the new score is under 0")]
		public void itShouldNotIncrementScoreIfUnderZero()
		{
			scoreController.score = 10;
			
			scoreController.AlterScore(-20);
			
			Assert.That (scoreController.score == 10);
		}
	}
}
