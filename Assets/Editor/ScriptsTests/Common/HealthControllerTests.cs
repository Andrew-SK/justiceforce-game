﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using UnityEngine;

namespace JusticeForceTests
{
	[TestFixture]
	[Category("Health Controller Tests")]
	internal class HealthControllerTests
	{
		HealthController healthController = new HealthController();

		[Test]
		[Category("It should alter the health by the specified amount if the new health value is below the maximum health amount and above the minimum health amount")]
		public void itShouldAlterTheHealthIfInBoundaries()
		{
			healthController.SetHealth(50);

			healthController.AlterHealth(-20);

			Assert.That (healthController.GetHealth() == 30);
		}

		[Test]
		[Category("It should set the health to 0 if the 'new health' goes below zero")]
		public void itShouldSetHealthToZeroIfBelowZero()
		{
			healthController.SetHealth(1);
			
			healthController.AlterHealth(-2);
			
			Assert.That (healthController.GetHealth() == 0);
		}

		[Test]
		[Category("It should set the health to 100 if the 'new health' goes above 100")]
		public void itShouldSetHealthTo100IfAbove100()
		{
			healthController.SetHealth(100);
			
			healthController.AlterHealth(10);
			
			Assert.That (healthController.GetHealth() == 100);
		}
	}
}
