﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using NUnit.Framework;
using UnityEngine;

//namespace JusticeForceTests
//{
//	[TestFixture]
//	[Category("Game Controller Tests")]
//	internal class GameControllerTests
//	{
//
//		GameController gameController = new GameController();
//
//		[Test]
//		[Category("It should return a nonempty array of vehicle gameobjects")]
//		public void itShouldReturnSingleplayerVehicle()
//		{
//			
//			GameObject vehicle = gameController.setupSinglePlayerCar();
//
//			Assert.That(vehicle != null);
//			Assert.That(vehicle.GetComponent<LocalCarController>() != null, "vehicle has no LocalCarController component");
//		}
//
//		[Test]
//		[Category("It should return an array of 2 vehicle gameobjects one local one network")]
//		public void itShouldReturnSingleplayerAndNetworkVehicle()
//		{
//			GameObject[] vehicles;
//
//			gameController.prefab_cop = new GameObject();
//			gameController.prefab_robber = new GameObject();
//
//			if (MultiplayerController._entityList != null && 
//				MultiplayerController._entityList.Count > 0)
//			{
//				MultiplayerController._entityList.Clear();
//			}
//				
//
//			vehicles = gameController.setupPlayerCars();
//
//			Assert.That(vehicles.Length == 2, "returned array not equal to 2");
//			Assert.That(vehicles[0] != null, "robber is returned as null");
//			Assert.That(vehicles[1] != null, "cop is returned as null");
//			Assert.That(vehicles[0].GetComponent<LocalCarController>() != null || vehicles[1].GetComponent<LocalCarController>() != null, "no local car returned");
//			Assert.That(vehicles[0].GetComponent<NetworkVehicleController>() != null || vehicles[1].GetComponent<NetworkVehicleController>() != null, "no Network car returned");
//		}
//
//		[Test]
//		[Category("It should spawn a vehicle with the provided args")]
//		public void itShouldReturnVehicleGameObject()
//		{
//			GameObject vehicle;
//
//			gameController.prefab_cop = new GameObject();
//			gameController.prefab_robber = new GameObject();
//			if (MultiplayerController._entityList != null &&
//				MultiplayerController._entityList.Count > 0)
//			{
//				MultiplayerController._entityList.Clear();
//			}
//
//			// spawn local vehicle
//			vehicle = gameController.spawnVehicle(true, gameController.prefab_robber, new Vector3(0, 0, 0));
//
//			Assert.That(vehicle != null, "returned vehicle is null");
//			Assert.That(vehicle.GetComponent<NetworkVehicleController>() == null, "returned vehicle has network controller");
//			Assert.That(vehicle.GetComponent<LocalCarController>() != null, "returned vehicle has no local controller");
//		}
//	}
//}
