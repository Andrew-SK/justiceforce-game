using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using NUnit.Framework;
using UnityEngine;

namespace JusticeForceTests
{
	[TestFixture]
	[Category("Multiplayer Controller Tests")]
	internal class MultiplayerControllerTests
	{

		[Test]
		[Category("It should turn a physics message into a byte array")]
		public void itShouldBuildPhysicsMessageByteArray()
		{
			MultiplayerController mp_ctrl = MultiplayerController.Instance;

			byte messageVersion = (byte)1;
			byte messageType = (byte)'P';

			byte [] messageToSend = {};

			int id = 10;
			float health = 60.0f;
			Vector3 position = new Vector3(0,5,-1);
			Vector3 velocity = new Vector3(3,-1,0);
			Quaternion rotation = new Quaternion(1,5,5,-1);

			messageToSend = mp_ctrl.sendNetworkPhysicsUpdate(id,health,position,velocity,rotation);

			Assert.That(messageToSend.Length == 30, "message length: " + messageToSend.Length);
			Assert.That(messageToSend[0] == messageVersion);
			Assert.That (messageToSend[1] == messageType);
			Assert.That (messageToSend.Skip(2).Take(4).SequenceEqual(System.BitConverter.GetBytes(health)));
			Assert.That (messageToSend.Skip(6).Take(4).SequenceEqual(System.BitConverter.GetBytes(position.x)));
			Assert.That (messageToSend.Skip(10).Take(4).SequenceEqual(System.BitConverter.GetBytes(position.y)));
			Assert.That (messageToSend.Skip(14).Take(4).SequenceEqual(System.BitConverter.GetBytes(velocity.x)));
			Assert.That (messageToSend.Skip(18).Take(4).SequenceEqual(System.BitConverter.GetBytes(velocity.y)));
			Assert.That (messageToSend.Skip(22).Take(4).SequenceEqual(System.BitConverter.GetBytes(rotation.eulerAngles.z)));
			Assert.That (messageToSend.Skip(26).Take(4).SequenceEqual(System.BitConverter.GetBytes(id)));
		}

		[Test]
		[Category("It should turn a death message to byte array")]
		public void itShouldBuildDeathMessageByteArray()
		{
			MultiplayerController mp_ctrl = MultiplayerController.Instance;

			byte messageVersion = (byte)1;
			byte messageType = (byte)'D';

			byte[] messageToSend = {};

			int id = 10;

			messageToSend = mp_ctrl.sendDeathMessage (id);

			Assert.That(messageToSend.Length == 6);
			Assert.That (messageToSend[0] == messageVersion);
			Assert.That (messageToSend[1] == messageType);
			Assert.That (messageToSend.Skip(2).Take (4).SequenceEqual(System.BitConverter.GetBytes (id)));
		}

		[Test]
		[Category("It should turn an Item Pickup Message to byte array")]
		public void itShouldBuildItemPickupMessageByteArray() 
		{
			MultiplayerController mp_ctrl = MultiplayerController.Instance;

			byte messageVersion = (byte)1;
			byte messageType = (byte)'I';
			
			byte[] messageToSend = {};
			
			int item_id = 10;
			int player_id = 10;

			messageToSend = mp_ctrl.sendItemPickupMessage (item_id, player_id);

			Assert.That(messageToSend.Length == 10);
			Assert.That (messageToSend[0] == messageVersion);
			Assert.That (messageToSend[1] == messageType);
			Assert.That (messageToSend.Skip(2).Take (4).SequenceEqual(System.BitConverter.GetBytes (item_id)));
			Assert.That (messageToSend.Skip(6).Take (4).SequenceEqual(System.BitConverter.GetBytes (player_id)));
		}

		[Test]
		[Category("It should turn a Rocket Fire Message to byte array")]
		public void itShouldBuildRocketFireMessageByteArray() 
		{
			MultiplayerController mp_ctrl = MultiplayerController.Instance;
			
			byte messageVersion = (byte)1;
			byte messageType = (byte)'R';
			
			byte[] messageToSend = {};
			
			Vector3 position = new Vector3(0,3);
			Quaternion rotation = new Quaternion (0, 0, 5, 0);

			int rocket_id = 10;

			messageToSend = mp_ctrl.sendRocketFire (position, rotation, rocket_id);

			Assert.That(messageToSend.Length == 18);
			Assert.That (messageToSend[0] == messageVersion);
			Assert.That (messageToSend[1] == messageType);
			Assert.That (messageToSend.Skip(2).Take (4).SequenceEqual(System.BitConverter.GetBytes (position.x)));
			Assert.That (messageToSend.Skip(6).Take (4).SequenceEqual(System.BitConverter.GetBytes (position.y)));
			Assert.That (messageToSend.Skip(10).Take (4).SequenceEqual(System.BitConverter.GetBytes (rotation.eulerAngles.z)));
			Assert.That (messageToSend.Skip(14).Take (4).SequenceEqual(System.BitConverter.GetBytes (rocket_id)));
		}
	}
}
