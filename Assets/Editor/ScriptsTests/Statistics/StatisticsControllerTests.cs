﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using UnityEngine;

namespace JusticeForceTests
{
	[TestFixture]
	[Category("Statistics Controller Tests")]
	internal class StatisticsControllerTests
	{

		[Test]
		[Category("It should expect current round to be defined when initialise a new round")]
		public void itShouldInitialiseANewRound()
		{
			StatisticsController statisticsController = new StatisticsController();
			StatisticsController.controller = new StatisticsController();

			statisticsController.InitialiseRound();

			Assert.That (statisticsController.currentRound != null);
		}

		[Test]
		[Category("It should append the previous round to the game statistics")]
		public void itShouldAppendPreviousRoundToGameStatistics()
		{
			StatisticsController statisticsController = new StatisticsController();
			StatisticsController.controller = new StatisticsController();

			StatisticsController.roundStartTime = DateTime.Now;
			StatisticsController.roundStartScore = 20;

			statisticsController.currentRound = new RoundStatistic();
			statisticsController.currentRound.mapSeed = "123";
			statisticsController.currentRound.score = 40;

			statisticsController.playerStatistic = new PlayerStatistic();
			statisticsController.playerStatistic.gameStatistic = new GameStatistic("123", DateTime.Now);
			
			statisticsController.InitialiseRound();
			
			Assert.That (statisticsController.playerStatistic.gameStatistic.roundStatistics.Count == 1);
		}

		[Test]
		[Category("It should correctly set the round start score based from previous round")]
		public void itShouldCorrectlySetTheStartScoreForTheRound()
		{
			StatisticsController statisticsController = new StatisticsController();
			StatisticsController.controller = new StatisticsController();
			
			StatisticsController.roundStartTime = DateTime.Now;
			StatisticsController.roundStartScore = 20;
			
			statisticsController.currentRound = new RoundStatistic();
			statisticsController.currentRound.mapSeed = "123";
			statisticsController.currentRound.score = 40;

			statisticsController.playerStatistic = new PlayerStatistic();
			statisticsController.playerStatistic.gameStatistic = new GameStatistic("123", DateTime.Now);
			
			statisticsController.InitialiseRound();
			
			Assert.That (StatisticsController.roundStartScore == 40);
		}
	}
}
