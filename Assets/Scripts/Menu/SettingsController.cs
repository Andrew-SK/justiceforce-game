﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class SettingsController : MonoBehaviour {

    public float vol_amount = 0.5f;


    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

	// This controller sets the volume of the game or in the case of mute - saves the current value,
	// sets it to zero (if enable) and returns the volume to the previous if disabled.

    public void changeVolume(float amount)
    {
        update_vol(amount);
    }

    public void mute(bool val)
    {
        if (val)
        {
            AudioListener.volume = 0.0f;
        }
        else
        {
            update_vol(vol_amount);
        }
    }

    private void update_vol(float amount) {
        vol_amount = amount;
        AudioListener.volume = vol_amount;
    }
}
