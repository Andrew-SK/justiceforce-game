/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

    void Start()
    {
        MultiplayerController.Instance.SilentLogin();
    }

    public void toStatistics(string levelName)
    {
        Application.LoadLevel("Statistics");
    }

    public void toLeaderboard(string levelName)
    {
        MultiplayerController.Instance.LeaderBoard();
    }

    public void toSettings(string levelName)
    {
        Application.LoadLevel("Settings");
    }

    public void toHelp(string levelName)
    {
        Application.LoadLevel("Help");
    }

    public void toSinglePlayer(string levelName)
    {
        Application.LoadLevel("LevelOne");
    }

    public void toMultiPlayer(string levelName)
    {
        Application.LoadLevel("MatchMaking");
    }

    public void ToMainMenu(string levelName)
    {
        Application.LoadLevel("Main Menu");
    }

    public void ToLogin()
    {
        MultiplayerController.Instance.SilentLogin();
    }

    public void ToLogout()
    {
        MultiplayerController.Instance.LogOut();
    }

    // this will need to be updated as game progresses 
    public void PlayAgain(string levelName)
    {
        //Application.LoadLevel("main");
    }
}
