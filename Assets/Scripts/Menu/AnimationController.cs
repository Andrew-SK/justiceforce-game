﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

// This controller plays the animations seen in the main menu, based on the clicks of menu buttons.
// Animations are controlled by the overarching Animator component of the MainMenu Scene.
public class AnimationController : MonoBehaviour {

	public static bool gameStarted = false;
	public GameObject MainMenu;
    
    void Awake () {
        Animator anim = MainMenu.GetComponent<Animator>();
        if (!gameStarted) {
            MainMenu.GetComponent<Animator>().enabled = true;
        } else {
			MainMenu.GetComponent<Animator>().enabled = false;
		}
	}


    public void toSettings() {
        Animator anim = MainMenu.GetComponent<Animator>();   
            anim.Play("EnterSettings");
           
    }

    public void exitSettings() {
        Animator anim = MainMenu.GetComponent<Animator>();
        anim.Play("ExitSettings");
    }

    public void toHelp()
    {
        Animator anim = MainMenu.GetComponent<Animator>();
        anim.Play("EnterHelp");
        
    }

    public void exitHelp()
    {
        Animator anim = MainMenu.GetComponent<Animator>();
        anim.Play("ExitHelp");
    }

    public void toStatistics()
    {
        Animator anim = MainMenu.GetComponent<Animator>();
        anim.Play("EnterStatistics");
        
    }

    public void exitStatistics()
    {
        Animator anim = MainMenu.GetComponent<Animator>();
        anim.Play("ExitStatistics");
    }

    public void toMatchmaking()
    {
        Animator anim = MainMenu.GetComponent<Animator>();
        anim.Play("EnterMatchmaking");

    }

    public void exitMatchmaking()
    {
        Animator anim = MainMenu.GetComponent<Animator>();
        anim.Play("ExitMatchmaking");
    }

}
