﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class roomCleanup : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Resources.UnloadUnusedAssets();

    }
	
	// Update is called once per frame
	void Update () {

	}
    public void toExit() {
        MultiplayerController.Instance.leaveRoom();
    }
}
