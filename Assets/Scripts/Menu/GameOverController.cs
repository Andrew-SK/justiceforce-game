﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class GameOverController : MonoBehaviour {

	public StatisticsController statisticsController;
	
	void Start () {
		statisticsController = GameObject.Find ("StatisticsController").GetComponent<StatisticsController>();
		statisticsController.SubmitStatistics ();
	}

	void Update () {
		Destroy(GameObject.Find ("ScoreController"));
	}
}
