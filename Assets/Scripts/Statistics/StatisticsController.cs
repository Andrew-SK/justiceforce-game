﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Globalization;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi.Multiplayer;

/// <summary>
/// Statistics Controller
/// </summary>
public class StatisticsController : MonoBehaviour {

	// Singleton controller
	public static StatisticsController controller;

	// UI Text elements
	public Text score;
	public Text time;

	// Statistics submission module and score controller module
	public SubmitStatistics submitStatistics;
	public ScoreController scoreController;

	// Getter and setter to obtain player statistics
	public PlayerStatistic playerStatistic { get; set; }
	// Getter and setter to obtain current round statistics
	public RoundStatistic currentRound {get; set; }
	// Starting round time and score
	public static DateTime roundStartTime;
	public static int roundStartScore;

	// Use this for initialization
	void Awake () {
		// If no singleton controller is defined, then assign the 
		// controller to 'this' and set the round start score and time
		// to 0 and the current time respectively. Otherwise, if there
		// is a controller defined, destroy it.
		if (controller == null) {
			DontDestroyOnLoad(gameObject);

			controller = this;
			roundStartScore = 0;
			roundStartTime = DateTime.Now;

			InitialisePlayerStatistics ();
		} else if (controller != this) {
			Destroy(gameObject);
			this.playerStatistic = controller.playerStatistic;
			this.currentRound = controller.currentRound;
		}

		// Initially set isSubmitting to false
		StatisticsService.isSubmitting = false;

		// Initialise a round 
		InitialiseRound ();
	}

	/// <summary>
	/// Initialises the player statistics
	/// </summary>
	private void InitialisePlayerStatistics () {
		if (PlayGamesPlatform.Instance.localUser.userName.Length > 0) {
			string PlayId = PlayGamesPlatform.Instance.localUser.id;
			string PlayerName = PlayGamesPlatform.Instance.localUser.userName;
			string GameId = MultiplayerController._host.ParticipantId;
			DateTime localDate = DateTime.Now;
			GameStatistic gameStatistic = new GameStatistic (GameId, localDate);
			
			playerStatistic = new PlayerStatistic ();
			playerStatistic.playId = PlayId;
			playerStatistic.playerName = PlayerName;
			playerStatistic.gameStatistic = gameStatistic;
		} else {
			Debug.LogError("Error: cannot initialise player statistics");
		}
	}

	/// <summary>
	/// Initialises the round.
	/// </summary>
	public void InitialiseRound () {
		if (currentRound != null) {
			RoundStatistic newRound = new RoundStatistic();
			DateTime now = DateTime.Now;
			TimeSpan elapsedTime = now.Subtract(roundStartTime);

			newRound.timeElapsed = elapsedTime.Seconds;
			newRound.mapSeed = currentRound.mapSeed;
			newRound.score = currentRound.score - roundStartScore;

			playerStatistic.gameStatistic.addRound(newRound);

			roundStartScore = currentRound.score;
		} 

		currentRound = new RoundStatistic ();
		roundStartTime = DateTime.Now;
	}

	/// <summary>
	/// Sets score UI text when the round has finished
	/// </summary>
	public void RoundFinish () {
		score.text = currentRound.score.ToString();
	}

	/// <summary>
	/// Submits the statistics whent the game concludes
	/// </summary>
	public void SubmitStatistics () {
		InitialiseRound ();

		scoreController = GameObject.Find ("ctrl_Score").GetComponent<ScoreController>();
		playerStatistic.gameStatistic.score = scoreController.score;

		submitStatistics = GameObject.Find ("SubmitStatistics").GetComponent<SubmitStatistics>();
		submitStatistics.SubmitPlayerStatistics (playerStatistic);

		controller = null;
	}
}
