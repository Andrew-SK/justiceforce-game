﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

/// <summary>
/// Game Statistic Class
/// </summary>
public class GameStatistic {

	[JsonProperty("game_id")]
	public string gameId {get; set;}
	[JsonProperty("opponent_id")]
	public string opponentId {get; set;}
	[JsonProperty("date_time")]
	public DateTime dateTime {get; set;}
	[JsonProperty("score")]
	public int score {get; set;}
	[JsonProperty("winner_id")]
	public string winnerId {get; set;}
	[JsonProperty("winner_name")]
	public string winnerName {get; set;}
	[JsonProperty("round_statistics")]
	public ArrayList roundStatistics {get; set;}

	/// <summary>
	/// Initializes a new instance of the <see cref="GameStatistic"/> class.
	/// </summary>
	/// <param name="gameId">Game Identifier</param>
	/// <param name="dateTime">DateTime</param>
	public GameStatistic(string gameId, DateTime dateTime) {
		roundStatistics = new ArrayList();
		this.gameId = gameId;
		this.dateTime = dateTime;
	}	

	public void addRound(RoundStatistic roundStatistic) {
		roundStatistics.Add(roundStatistic);
	}
}
