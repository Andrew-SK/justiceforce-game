﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


/// <summary>
/// Submit Statistics Module
/// </summary>
public class SubmitStatistics : MonoBehaviour {

	// UI Text elements to display when game is over
	public Text winner;
	public Text score;
	public Text time;

	StatisticsService statisticsService;

	/// <summary>
	/// Submits the player statistics to the API and Google Play leaderboard
	/// </summary>
	public void SubmitPlayerStatistics(PlayerStatistic playerStatistic) {
		// Set isSubmitting to true
		StatisticsService.isSubmitting = true;

		statisticsService = gameObject.AddComponent<StatisticsService>();

		// Submit player score to the player's Google Play leaderboard
		SubmitToLeaderboard(playerStatistic.gameStatistic.score);

		// Serialise the player's statistics into JSON format
		string playerStatisticsJSON = JsonConvert.SerializeObject(playerStatistic, new IsoDateTimeConverter());

		// Request player statistics
		statisticsService.RequestPlayerStatistics(new Player(playerStatistic.playId, playerStatistic.playerName), value => {
			// Insert the player's new game statistics
			statisticsService.InsertPlayerStatisticsViaHTTP(playerStatisticsJSON, (done) => {
				// Wait 2 seconds, then display the player's statistics on the game screen
				StartCoroutine(Wait ((res) => {
					statisticsService.RequestPlayerStatistics(new Player(playerStatistic.playId, playerStatistic.playerName), gameStats => {
						// Set the winner name from the game
						string winnerName = gameStats[gameStats.Count - 1].winnerName;
					
						// If the winner didn't leave the game, set the winner name to the winner.
						if (winner != null) {
							winner.text = winnerName; 
						} else {
							winner.text = playerStatistic.playerName;
						}

						// Set the score UI text element to the score.
						score.text = gameStats[gameStats.Count - 1].score.ToString();
					});
				}));
			});
		});
	}

	/// <summary>
	/// Submits score to the Google Play leaderboard
	/// </summary>
	void SubmitToLeaderboard(int score) {
		Social.ReportScore(score, MultiplayerController.LeaderboardId, (bool success) => {
			if (success) {
				Debug.Log ("Score successfully posted to leaderboard!");
			} else {
				Debug.Log ("Score unsuccessfully posted to leaderboard!");
			}
		});
	}

	IEnumerator Wait(System.Action<bool> cb) {
		yield return new WaitForSeconds(2);
		cb(true);
	}
}
