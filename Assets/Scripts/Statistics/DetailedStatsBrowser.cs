﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */

using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi.Multiplayer;

/// <summary>
/// 	Detailed Stats Browser MonoBehaviour class
/// </summary>
public class DetailedStatsBrowser : MonoBehaviour {
	public static string baseWebAppUrl = "http://www.justiceforce-game.net/#/";

	/// <summary>
	/// 	Opens up a web browser and naviates to the logged in player's statistics.
	/// </summary>
	public void GoToDetailedStats() {
		Application.OpenURL (baseWebAppUrl + "statistics/player/" + PlayGamesPlatform.Instance.localUser.id);
	}
}
