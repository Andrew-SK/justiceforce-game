﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi.Multiplayer;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

/// <summary>
/// Statistics Service
/// </summary>
public class StatisticsService : MonoBehaviour {

	// Base API Url
	public static string baseApiUrl = "http://104.236.147.165/api/v1/";
	// Initially set isSubmitting to false
	public static bool isSubmitting = false;

	WWW wwwGetPlayerStatistics;

	// Path to get player statistics 
	string playerStatisticsUrl = baseApiUrl + "statistics/player/";
	// Path to get and create game statistics
	string gameStatisticsUrl = baseApiUrl + "statistics/game";
	// Path to get player details
	string playersUrl = baseApiUrl + "players/";

	string playerStatisticUrl;
	Player player;

	/// <summary>
	/// Gets the player details
	/// </summary>
	/// <returns>The player.</returns>
	public Player GetPlayerIdAndName () {
		if (PlayGamesPlatform.Instance.localUser.userName.Length > 0) {
			return new Player(PlayGamesPlatform.Instance.localUser.id, PlayGamesPlatform.Instance.localUser.userName);
		} 

		return null;
	}

	/// <summary>
	/// Requests the player statistics.
	/// </summary>
	/// <param name="player">The Player Details</param>
	/// <param name="cb">Callback when the server returns data</param>
	public void RequestPlayerStatistics (Player player, System.Action<List<GameStatistic>> cb) {
		this.player = player;
		playerStatisticUrl = playerStatisticsUrl + player.id;
		wwwGetPlayerStatistics = new WWW(playerStatisticUrl);

		StartCoroutine(RetrieveStatistics(wwwGetPlayerStatistics, value => {
			cb(value);
		}));
	}

	/// <summary>
	/// Retrieves player statistics from the API
	/// </summary>
	IEnumerator RetrieveStatistics(WWW www, System.Action<List<GameStatistic>> cb) {
		yield return www;

		// check for errors
		if (www.error == null)
		{
			JSONNode reqJSON = JSON.Parse(www.text); 

			if (reqJSON["error"].ToString().Length != 0) {
				Debug.Log ("Player (" + player.id + ") not found - creating new player");

				CreatePlayerAndRetrieveStatistics (value => {
					cb (value);
				});
			} else if (reqJSON.Count != 0) {
				Debug.Log ("Player (" + player.id + ") found! Retrieving statistics");

				List<GameStatistic> gameStatistics = JsonConvert.DeserializeObject<List<GameStatistic>>(reqJSON["game_statistics"].ToString());

				cb (gameStatistics);
			}
		} else {
			Debug.Log("error: "+ www.error);
		} 
	}

	/// <summary>
	/// Creates a player and retrieves their statistics
	/// </summary>
	void CreatePlayerAndRetrieveStatistics (System.Action<List<GameStatistic>> cb)
	{
		WWW wwwPost;
		UTF8Encoding encoding = new UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();
		string json = JsonConvert.SerializeObject(player);

		postHeader.Add ("Content-Type", "application/json");

		wwwPost = new WWW (playersUrl, encoding.GetBytes (json), postHeader);
		StartCoroutine (InsertPlayerAndRetrieveStatistics (wwwPost, value => {
			cb (value);
		}));
	}

	/// <summary>
	/// Inserts the player and retrieve statistics.
	/// </summary>
	IEnumerator InsertPlayerAndRetrieveStatistics(WWW www, System.Action<List<GameStatistic>> cb)
	{
		yield return www;

		if (www.error == null){
			Debug.Log("WWW Ok!: " + www.text);

			RequestPlayerStatistics (player, value => {
				cb (value);
			});
		} else {
			Debug.Log("WWW Error: "+ www.error);
		}    
	} 

	/// <summary>
	/// Inserts the player statistics via a HTTP POST request to the API
	/// </summary>
	public void InsertPlayerStatisticsViaHTTP (string playerStatisticsJSON, System.Action<bool> cb) {
		WWW wwwPost;
		UTF8Encoding encoding = new UTF8Encoding ();
		Dictionary<string, string> postHeader = new Dictionary<string, string> ();

		postHeader.Add ("Content-Type", "application/json");

		wwwPost = new WWW (gameStatisticsUrl, encoding.GetBytes (playerStatisticsJSON), postHeader);

		StartCoroutine(InsertPlayerStatisticsRequest(wwwPost, (done) => {
			cb(true);
		}));

	}

	/// <summary>
	/// Inserts the player statistics 
	/// </summary>
	IEnumerator InsertPlayerStatisticsRequest(WWW www, System.Action<bool> cb) {
		yield return www;

		if (www.error == null) {
			cb(true);
			Debug.Log ("Player's statistics successfully inserted!");
		} else {
			cb(true);
			Debug.Log ("Error: Player's statistics not successfully inserted [" + www.error + "]");
		}
	}
}
