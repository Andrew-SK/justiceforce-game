﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

/// <summary>
/// Player Class
/// </summary>
public class Player {
	[JsonProperty ("play_id")]
	public string id {get; set;}
	[JsonProperty ("player_name")]
	public string name {get; set;}

	/// <summary>
	/// Initializes a new instance of the <see cref="Player"/> class.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="name">Name.</param>
	public Player(string id, string name) {
		this.id = id;
		this.name = name;
	}
}
