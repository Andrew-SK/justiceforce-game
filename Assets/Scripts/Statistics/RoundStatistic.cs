﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

/// <summary>
/// Round Statistic Class
/// </summary>
public class RoundStatistic {
	[JsonProperty("time_elapsed")]
	public int timeElapsed {get; set;}
	[JsonProperty("map_seed")]
	public string mapSeed {get; set;}
	[JsonProperty("score")]
	public int score {get; set;}
	[JsonProperty("damage_dealt")]
	public int damageDealt {get; set;}
	[JsonProperty("item_statistics")]
	public ArrayList itemStatistics {get; set;}

	/// <summary>
	/// Initializes a new instance of the <see cref="RoundStatistic"/> class.
	/// </summary>
	public RoundStatistic() {
		itemStatistics = new ArrayList();
	}

	/// <summary>
	/// Adds item to the item statistic list
	/// </summary>
	/// <param name="itemStatistic">Item statistic.</param>
	/// <param name="timePicked">Time picked.</param>
	public void addItem(ItemStatistic itemStatistic, int timePicked) {
		itemStatistic.timePicked = timePicked;
		itemStatistics.Add (itemStatistic);
	}
}
