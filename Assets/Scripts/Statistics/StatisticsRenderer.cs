﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi.Multiplayer;
using System.Text;

/// <summary>
/// Statistics Renderer Class
/// </summary>
public class StatisticsRenderer : MonoBehaviour {
	
	PlayerStatistic playerStatistics;
	List<GameStatistic> gameStatistics;

	public StatisticsService statisticsController;
	public GameObject statTitles;
	public GameObject errorMessage;
	public GameObject viewMore;
	public Text playerTitle;
	public Text totalPoints;
	public Text highestGameScore;
	public Text highestGameTime;
	public Text totalTimePlayed;
	public Text gamesPlayed;
	public Text gamesWon;


	// Use this for initialization
	public void LoadPlayerStats () {
		playerStatistics = new PlayerStatistic();

		Player player = statisticsController.GetPlayerIdAndName ();

		// If the player is successfully retrieved, then request their
		// statistics (obtain statistics from the API) and calculate and display 
		// the statistics summary 
		if (player != null) {
			statisticsController.RequestPlayerStatistics(player, value => {
				gameStatistics = value;
				playerStatistics.calculateStatistics (player.id, gameStatistics);
				RenderPlayerStatistics();	
			});
		} else {
			errorMessage.SetActive (true);
		}
	}

	/// <summary>
	/// Renders the player statistics
	/// </summary>
	void RenderPlayerStatistics () {
		statTitles.SetActive (true);
		viewMore.SetActive(true);

		playerTitle.text = PlayGamesPlatform.Instance.localUser.userName.ToString() + "'s";
		
		totalPoints.text = playerStatistics.totalPoints.ToString ();
		gamesPlayed.text = playerStatistics.gamesPlayed.ToString ();
		highestGameScore.text = playerStatistics.highestGameScore.ToString ();
		highestGameTime.text = playerStatistics.highestGameTime.ToString () + " s";
		totalTimePlayed.text = playerStatistics.totalTimePlayed.ToString () + " s";
		gamesWon.text = playerStatistics.totalGamesWon.ToString ();
	}
}
