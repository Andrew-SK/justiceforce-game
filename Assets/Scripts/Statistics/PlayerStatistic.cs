﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using Newtonsoft.Json;

/// <summary>
/// Player Statistic Class
/// </summary>
public class PlayerStatistic {

	[JsonProperty("play_id")]
	public string playId {get; set;}
	[JsonProperty("player_name")]
	public string playerName {get; set;}
	[JsonProperty("game_statistics")]
	public GameStatistic gameStatistic {get; set;}

	public int totalPoints {get; set;}
	public int highestGameScore {get; set;}
	public int gamesPlayed {get; set;}
	public int highestGameTime {get; set;}
	public int totalTimePlayed {get; set;}
	public int totalGamesWon {get; set;}

	/// <summary>
	/// Initializes a new instance of the <see cref="PlayerStatistic"/> class.
	/// </summary>
	public PlayerStatistic() {}

	/// <summary>
	/// Calculates a summary of statistics for all player games
	/// </summary>
	/// <param name="playerId">Player identifier.</param>
	/// <param name="gameStatistics">Game statistics.</param>
	public void calculateStatistics (string playerId, List<GameStatistic> gameStatistics) {
		ArrayList roundStatistics;

		int totalPoints = 0, 
			totalTime = 0,
			maxScore = 0, 
			maxGameTime = 0,
			gamesWon = 0,
			gameScore,
			totalRoundTime,
			roundTime,
			score;

		foreach (GameStatistic gameStatistic in gameStatistics) {
			roundStatistics = gameStatistic.roundStatistics;
			score = gameStatistic.score;
			totalRoundTime = 0;
			gameScore = 0;

			for (int i = 0; i < roundStatistics.Count; i++) {
				RoundStatistic roundStatistic = JsonConvert.DeserializeObject<RoundStatistic>(roundStatistics[i].ToString());
				roundTime = roundStatistic.timeElapsed;

				totalRoundTime += roundTime;
				totalTime += roundTime;
			}

			totalPoints += score;

			if (score > maxScore) maxScore = score; 
			if (totalRoundTime > maxGameTime) maxGameTime = totalRoundTime;
			if (gameStatistic.winnerId.Equals(playerId)) gamesWon++;
		}

		this.totalPoints = totalPoints;
		this.highestGameScore = maxScore;
		this.gamesPlayed = gameStatistics.Count;
		this.highestGameTime = maxGameTime;
		this.totalTimePlayed = totalTime;
		this.totalGamesWon = gamesWon;
	}
}
