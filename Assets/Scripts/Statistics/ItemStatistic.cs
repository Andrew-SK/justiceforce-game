﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */

using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

/// <summary>
/// Item Statistic Class
/// </summary>
public class ItemStatistic {
	[JsonProperty("item_name")]
	public string itemName {get; set;}
	[JsonProperty("time_picked")]
	public int timePicked {get; set;}
	[JsonProperty("time_used")]
	public int timeUsed {get; set;}

	/// <summary>
	/// Initializes a new instance of the <see cref="ItemStatistic"/> class.
	/// </summary>
	/// <param name="itemName">Item name.</param>
	/// <param name="timePicked">Time picked.</param>
	public ItemStatistic (string itemName, int timePicked) {
		this.itemName = itemName;
		this.timePicked = timePicked;
	}
}
