﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {
	GameObject prefab;

	public Item(float x, float y, GameObject prefab) {
		float xPos = Tile.TILE_SIZE * x + ItemOffset();
		float yPos = Tile.TILE_SIZE * y + ItemOffset();

		InstantiateItem(prefab, new Vector3(xPos, yPos, -0.5f));
	}

	GameObject InstantiateItem(GameObject prefab, Vector3 position) {
		GameObject item = (GameObject) Instantiate (prefab, position, Quaternion.identity);
		MultiplayerController.Instance.addToItemList (item.GetComponent<ItemController>());
		item.transform.SetParent (GameObject.Find ("ctrl_Map").transform);
		
		return item;
	}

    private float ItemOffset()
    {
        return UnityEngine.Random.Range(-Tile.TILE_SIZE / 4, Tile.TILE_SIZE / 4);
    }
}
