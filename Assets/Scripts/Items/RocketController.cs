﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class RocketController : MonoBehaviour {
	public int speed = 10;

	void OnCollisionEnter2D(Collision2D other) {
		other.gameObject.GetComponent<HealthController> ().AlterHealth (25);
		Destroy(gameObject);
	}	

	void FixedUpdate() {
		Vector3 newPos = gameObject.transform.position;
		newPos = newPos + speed * gameObject.transform.up * Time.deltaTime;

		gameObject.transform.position = newPos;
	}

}