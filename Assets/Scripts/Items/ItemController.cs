﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;


public class ItemController : MonoBehaviour {
	public string name;
	public int value;
	public int list_id { get; set; }

	public override int GetHashCode() {
		return (int)Mathf.Pow((float)gameObject.transform.position.x, (float)gameObject.transform.position.y);
	}
}
