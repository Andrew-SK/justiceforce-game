﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class mapController : MonoBehaviour {


	public static int TRY_THRESH = 20;
	public static float POS_BOUND = 1f/4;
	public static int ITEM_GEN_LOWER = 1, ITEM_GEN_UPPER = 5;

	public int numberOfTiles;
	public int mapSize;
	public GameObject prefab_start;
	public GameObject prefab_exit;
	public GameObject prefab_wall;
	public GameObject prefab_road;
	public GameObject[] prefab_dwellings;
	public GameObject[] prefab_items;
	public Tile[,] roadPath;
    public int ITEM_SPAWN_CHANCE; //out of 100
    public int DWELLING_SPAWN_CHANCE; // out of 100

	public StatisticsController statisticsController;


    private float tile_size;

	// Use this for initialization
	void Start () {
		statisticsController = GameObject.Find("StatisticsController").GetComponent<StatisticsController>();

        try
        {
			int hostId = MultiplayerController.Instance.hostId();
			UnityEngine.Random.seed = hostId;
			statisticsController.currentRound.mapSeed = hostId.ToString();
        }
        catch {
            Debug.Log("hostId is null");
        }
        
        NewMap(100);
    }

    void NewMap(int PATH_LENGTH)
    {

        List<char> current_choices = new List<char>(new char[] { 'u', 'l', 'r', 'd' });
        
        Step current = new Step(0, 0, 's');
        Step previous = null;
        List<Step> path = new List<Step>();
		Step.road_dict = new Dictionary<Step, char>();
        path.Add(current);

        Step.road_dict.Add(current, current.dir);
	
        while (path.Count < PATH_LENGTH)
        {
            current = path[path.Count - 1];

            try
            {
                previous = path[path.Count - 1];
            }
            catch {
                Debug.Log("previous out of range (first step)");
            }


            Step target = choose_random_choice(current_choices, current, previous);
            while (!target.isValid && current_choices.Count > 0) {
                char val;
                if (!Step.road_dict.TryGetValue(target,out val))
                {
                    if (target.find_neighbours(current) <= 1)
                    {

                        target.isValid = true;
                    }

                    else {
                        Step.road_dict[target] = 'n';                      
                    }
                }

                if (!target.isValid) {
                    current_choices.Remove(target.dir);
                    if(current_choices.Count != 0) { 
                        target = choose_random_choice(current_choices, current, previous);
                    }
                }
            }

            current_choices = new List<char>(new char[] { 'u', 'l', 'r' ,'d'  });

            if (target.isValid)
            {
                path.Add(target);
                Step.road_dict.Add(target, target.dir);
            }
            else
            {
                current_choices.Remove(path[path.Count - 1].dir);
                path.RemoveAt(path.Count - 1);
                Step.road_dict[path[path.Count - 1]] = 'n';
            }
                       

        }

        path[path.Count - 1].dir = 'e'; 
        generate_new_map(path);
		gen_walls(path);
    }

	private void generate_new_map(List<Step> map_steps) {

        for (int i = 0; i<map_steps.Count;i++) {
            gen_dir(i, map_steps);
        }

    }

    private void gen_walls(List<Step> map_steps) {

        List<Step> check_steps = new List<Step>();

        foreach (Step s in map_steps) {
            //up
            Step up = new Step(s.x,s.y,'-');
            up.y += 1;
            check_steps.Add(up);

            Step down = new Step(s.x, s.y, '-');
            down.y -= 1;
            check_steps.Add(down);

            Step left = new Step(s.x, s.y, '-');
            left.x -= 1;
            check_steps.Add(left);

            Step right = new Step(s.x, s.y, '-');
            right.x += 1;
            check_steps.Add(right);

            Step up_right = new Step(s.x, s.y, '-');
            up_right.y += 1;
            up_right.x += 1;
            check_steps.Add(up_right);

            Step up_left = new Step(s.x, s.y, '-');
            up_left.y += 1;
            up_left.x -= 1;
            check_steps.Add(up_left);

            Step down_right = new Step(s.x, s.y, '-');
            down_right.y -= 1;
            down_right.x += 1;
            check_steps.Add(down_right);

            Step down_left = new Step(s.x, s.y, '-');
            down_left.y -= 1;
            down_left.x -= 1;
            check_steps.Add(down_left);

            Debug.Log("s: " + s.x + "," + s.y + ": up: " + up.x + "," + up.y + " down:" + down.x + "," + down.y + " left:" + +left.x + "," + left.y + " right:" + right.x + "," + right.y);

            foreach (Step check in check_steps) {
                char val;
                
                if (!map_steps.Contains(check) && (!Step.road_dict.TryGetValue(check, out val) || val == 'n'))
                {
					generate_wall(check.x, check.y);
					Step.road_dict[check] = 'w';
                }
            }


        }


    }    

    private void gen_dir(int i,List<Step> path) {

        switch (path[i].dir) {
            case ('u'):
                generate_road(path[i-1].x,path[i-1].y+1);
                
                break;

            case ('d'):
                generate_road(path[i - 1].x, path[i - 1].y - 1);

                break;

            case ('l'):
                generate_road(path[i - 1].x -1, path[i - 1].y);

                break;

            case ('r'):
                generate_road(path[i - 1].x + 1, path[i - 1].y);

                               
                break;

            case ('s'):
                generate_start(0, 0);
                break;      

            case ('e'):
                //chage case for end direction;
                generate_end(path[i].x, path[i].y);
                break;
            
                

            default:
                break;

        }
    }

    private Tile generate_road(int x, int y)
    {
        Tile road;
        road =  new Tile(x, y, prefab_road);

        int do_spawn_item = UnityEngine.Random.Range(0, prefab_items.Length * (100/ITEM_SPAWN_CHANCE));

        if (do_spawn_item < prefab_items.Length)
        {
            new Item(x, y, prefab_items[do_spawn_item]);
        }

        return road;
    }
    private Tile generate_start(int x, int y) {
        Tile start;
        start = new Tile(x, y, prefab_start);
        return start;
    }

    private Tile generate_end(int x, int y)
    {
        Tile end;
        end = new Tile(x, y, prefab_exit);
        return end;
    }

    
    private Tile generate_wall(int x, int y)
    {
        int do_spawn_dwelling = UnityEngine.Random.Range(0, prefab_dwellings.Length*(100/DWELLING_SPAWN_CHANCE));
        Tile wall;

        if (do_spawn_dwelling > prefab_dwellings.Length -1)
        {
            wall = new Tile(x, y, prefab_wall);
        }
        else
        {
            wall = new Tile(x, y, prefab_dwellings[do_spawn_dwelling]);
        }
        return wall;
    }


    private Step choose_random_choice(List<char> current_choices, Step current, Step previous)
    {
        remove_opposite(current_choices, current);

        if (previous != null) {
            remove_opposite(current_choices, previous);
        }
        string c = new string(current_choices.ToArray());        
                
        char next_dir = current_choices[UnityEngine.Random.Range(0, current_choices.Count)];
        print("next_dir is: " + next_dir);
        Step next_step;

        switch (next_dir) { 
            case ('u'):
                next_step = new Step(current.x, current.y + 1, next_dir);
                break;
            case ('d'):
                next_step = new Step(current.x, current.y - 1, next_dir);
                break;
            case ('l'):
                next_step = new Step(current.x-1, current.y, next_dir);
                break;
            default:
                next_step = new Step(current.x+1, current.y, next_dir);
                break;
        }
        return next_step;
    }

    private void remove_opposite(List<char> current_choices, Step current)
    {
        switch (current.dir)
        {
            case ('u'):
                current_choices.Remove('d');
                break;
            case ('d'):
                current_choices.Remove('u');
                break;
            case ('l'):
                current_choices.Remove('r');
                break;
            case ('r'):
                current_choices.Remove('l');
                break;
        }

    }

    
}


class Step
{
    public char dir { get; set; }
    public bool isValid { get; set; }

    public int x { get; set; }
    public int y { get; set; }

    public override int GetHashCode()
    {
        return (int)Mathf.Pow((float)x, (float)y);
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType()) return false;
        Step s = (Step)obj;
        return (x == s.x) && (y == s.y);
    }



    static public Dictionary<Step, char> road_dict = new Dictionary<Step, char>();


    public int find_neighbours(Step current) {
        int num_neighbours = 0;

        Step[] neighbours = new Step[4] {
            new Step(x - 1, y, '-'),
            new Step(x + 1, y, '-'),
            new Step(x, y - 1, '-'),
            new Step(x, y + 1, '-')
        };

         foreach (Step s in neighbours) {
            if (road_dict.ContainsKey(s))
            {
                num_neighbours++;
            }
        }
//        Debug.Log("num neighbours :" + num_neighbours);
        return num_neighbours;
               
    }


    public Step(int x, int y,char dir) 
    {
        this.x = x;
        this.y = y;
        this.isValid = false;
        this.dir = dir;

    }
   
}
