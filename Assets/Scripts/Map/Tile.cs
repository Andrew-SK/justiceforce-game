﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {
	public static int TILE_SIZE = 5;
	public static int NUM_NEIGHBOURS = 4;

	public int x {get; set;}
	public int y {get; set;}
	public GameObject prefab {get; set;}
	public Vector3 position {get; set;}
	public bool exists {get; set;}
	public bool isRoad {get; set;}

	public Tile(int x, int y) {
		this.exists = false;

		this.x = x;
		this.y = y;
	}

	public Tile(int x, int y, GameObject prefab) {
		int xPos = TILE_SIZE * x;
		int yPos = TILE_SIZE * y;

		this.x = x;
		this.y = y;
		this.exists = true;
		this.isRoad = true;

		InstantiateTile(prefab, new Vector3(xPos, yPos, 0), 0);
	}

	public Tile(int x, int y, GameObject prefab, float rotation) {
		int xPos = TILE_SIZE * x;
		int yPos = TILE_SIZE * y;
		
		this.x = x;
		this.y = y;
		this.exists = true;
		
		InstantiateTile(prefab, new Vector3(xPos, yPos, 0), rotation);
	}

	GameObject InstantiateTile(GameObject prefab, Vector3 position, float rotation) {
		GameObject tile = (GameObject) Instantiate (prefab, position, Quaternion.identity);
		tile.transform.Rotate(new Vector3(0, 0, rotation));
		
		tile.transform.SetParent (GameObject.Find ("ctrl_Map").transform);
		
		return tile;
	} 
}
