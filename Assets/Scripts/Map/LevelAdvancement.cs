﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class LevelAdvancement : MonoBehaviour {
	
	public string NextLevel;

	void OnTriggerEnter2D(Collider2D other) {
		Application.LoadLevel (NextLevel);
	}

	public void toScene (string sceneName) {
		Application.LoadLevel (sceneName);
	}
}
