/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using System;
using System.Collections.Generic;
using UnityEngine;


namespace AssemblyCSharp
{
	public abstract class NetworkUpdatableGameObject : MonoBehaviour
	{
		public int list_id { get; set; }
		public abstract void receivePhysicsUpdate(float health, float posX, float posY, float velX, float velY, float rotZ);
		public abstract void sendNetworkPhysicsUpdate(string id, float health, Vector3 position, Vector3 velocity, Quaternion rotation);
	}
}

