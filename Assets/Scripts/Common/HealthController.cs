﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Health Controller
/// </summary>
public class HealthController : MonoBehaviour {

	// Renderer for the damage and health bar
	private SpriteRenderer damageBar;
	private SpriteRenderer healthBar;

	// Particle System for car smoke
    private ParticleSystem carSmoke;

	// Time to display the car health bar
	public float timeToDisplayHealthBar = 2f;

	bool showHealthBar = false;
	float fadeValue = 1f;
	float currentTime = 0f;
	float timeToFade = 0.5f;
	float healthBarScale;

	private static float MIN_HEALTH = 0.0f, MAX_HEALTH = 100.0f, SCALE_INTERVAL = 200.0f;
    private static float MAX_SMOKE_RATE = 50.0f;
    private float health = 100.0f;

	/// <summary>
	/// Upon initialisation of the Health Controller
	/// </summary>
	void Start () {
		damageBar = gameObject.transform.FindChild("sprt_DamageBar").GetComponent<SpriteRenderer>();
		healthBar = gameObject.transform.FindChild("sprt_HealthBar").GetComponent<SpriteRenderer>();
        carSmoke = gameObject.transform.FindChild("car_smoke").GetComponent<ParticleSystem>();
        healthBarScale = healthBar.transform.localScale.x;
	}

	/// <summary>
	/// Update per frame
	/// </summary>
	void Update () {
        alterSmoke();
        if (showHealthBar)
			FadeHealthBarIn();	

		if (health <= 0) // end the level
		{

			// unfortunately there is no nice way to determine if the vehicle is a local or 
			// a network controlled vehicle from this class, so we have to try both
			LocalCarController lcc;
			NetworkVehicleController nvc;
			int id;

			// get the list_id
			if ((lcc = gameObject.GetComponent<LocalCarController>()) != null) 
			{
				id = lcc.list_id;
			} else
			{
				nvc = gameObject.GetComponent<NetworkVehicleController>();
				id = nvc.list_id;
			}

			MultiplayerController.Instance.sendDeathMessage(id);
			GameObject.Find("GameController").GetComponent<GameController>().EndLevel();
		}
	}

	/// <summary>
	/// Alters the health on the UI
	/// </summary>
	/// <param name="amount">Health amount</param>
	void UI_AlterHealth (float amount) {
		healthBar.transform.localScale = new Vector3(healthBar.transform.localScale.x + amount * (healthBarScale / 100),
		                                             healthBar.transform.localScale.y,
		                                             healthBar.transform.localScale.z);
		
		healthBar.transform.Translate(new Vector3(amount / SCALE_INTERVAL, 0f, 0f));
	}

	/// <summary>
	/// Alters the health.
	/// </summary>
	/// <param name="amount">Health Amount</param>
	public void AlterHealth (float amount) {
		float newHealth = health + amount;
		
		// Ensure that health is above min health and below max health
		if (newHealth >= MIN_HEALTH && newHealth <= MAX_HEALTH) {
			health += amount;
		} else if (newHealth < MIN_HEALTH) {
			health = MIN_HEALTH;
		} else if (newHealth > MAX_HEALTH) {
			health = MAX_HEALTH;
		}

        
	}

	/// <summary>
	/// Alters the smoke emission rate
	/// </summary>
    private void alterSmoke()
    {
        carSmoke.emissionRate = MAX_SMOKE_RATE*((MAX_HEALTH - health)/MAX_HEALTH);
    }

	/// <summary>
	/// Alters the car health
	/// </summary>
	public void AlterUIHealth (float amount) {
		AlterHealth (amount);

		UI_AlterHealth (amount);
	}	

	/// <summary>
	/// Displays the health bar
	/// </summary>
	public void ShowHealthBar () {
		if (!showHealthBar) {
			showHealthBar = true;
		}
	}

	/// <summary>
	/// Fades health bar in
	/// </summary>
	void FadeHealthBarIn ()
	{
		currentTime += Time.deltaTime;

		if (currentTime <= timeToFade) {
			fadeValue = (currentTime / timeToFade);

			FadeHealthBarSprites(fadeValue);
		} else if (currentTime > timeToDisplayHealthBar) {
			showHealthBar = false;
			currentTime = 0;
			fadeValue = 1f;
			FadeHealthBarOut();
		} 
	}

	/// <summary>
	/// Fades health bar out
	/// </summary>
	void FadeHealthBarOut () {
		FadeHealthBarSprites(0);
	}

	/// <summary>
	/// Sets health bar fade colors
	/// </summary>
	void FadeHealthBarSprites(float fadeValue) {
		healthBar.color = new Color (0f, 255f, 33f / 255f, fadeValue);
		damageBar.color = new Color (255f, 0f, 0f, fadeValue);
	}

	/// <summary>
	/// Gets the health.
	/// </summary>
	public float GetHealth () {
		return health;
	}

	/// <summary>
	/// Sets the health.
	/// </summary>
	public void SetHealth (float health) {
		this.health = health;
	}
}
