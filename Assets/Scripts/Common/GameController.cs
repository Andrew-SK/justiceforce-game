/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using GooglePlayGames.BasicApi;
using System.Collections;
using System;

public class GameController : MonoBehaviour {

	// Initialize all variables and constants used

	public GameObject prefab_robber;
	public GameObject prefab_cop;

    private GameObject cop;
    private GameObject robber;
    private Rigidbody2D cop_rb;
    private Rigidbody2D robber_rb;

	public Vector3 robber_start;
	public Vector3 cop_start;

	public Sprite[] items;

    private float networkUpdateDelta = 0.1f;
    private float nextUpdateTime = 0;

    private float StartTime = 0;
    private float WaitTime = 3.0f;

    public StatisticsController statController;
    public StatisticsService statService;
    public GameObject StatisticsPanel;

    // Use this for initialization
    void Start() {
    	SetupMultiplayerGame();
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

		// update the first start time for the ReadySetGo sequence at the start of the game

        if (Time.time > nextUpdateTime)
        {
            nextUpdateTime = Time.time + networkUpdateDelta;

        }

        StartTime = Time.time;

		// Ensure that both the cop and robber ridgid bodies exist.

        try
        {
            cop_rb = cop.GetComponent<Rigidbody2D>();
            cop_rb.position = cop_start;
        }
        catch
        {
            Debug.Log("No cop on start");
        }

        try {
            robber_rb = robber.GetComponent<Rigidbody2D>();
            robber_rb.position = robber_start;
        }
        catch
        {
            Debug.Log("No robber on start");
        }

        
    }

    // Update is called once per frame
    void Update () {
        
	}

	// Occurs after Update()
	public void LateUpdate () {
		// if the game is still in the Ready-Set-Go phase, reset the Cop/Robber positions to start.
        if (Time.time < (StartTime + WaitTime))
        {
            Debug.Log("curTime: " + Time.time + " ST + WT: " + (StartTime + WaitTime));

            try
            {
                cop_rb.position = cop_start;
            }
            catch
            {
                Debug.Log("No cop");
            }
            try
            {
                robber_rb.position = robber_start;
            }
            catch
            {
                Debug.Log("No robber");
            }

            

        }


    }

	// Call correct game mode

	public GameObject SetupMultiplayerGame() {

		if (MultiplayerController.Instance.isInRoom)
		{
			setupPlayerCars();
		} else
		{
			setupSinglePlayerCar();
		}

		return robber;
		
	}

	private void setupSinglePlayerCar()
	{
		robber = spawnVehicle(true, prefab_robber, new Vector3(0, 0, -1));
	}

	private GameObject[] setupPlayerCars()
	{
		string hostVehicleName;
		string clientVehicleName;
		if (MultiplayerController.Instance.isHost) {
			// the host is going to be the Cop
			// the client is the robber
			hostVehicleName = "Cop";
			clientVehicleName = "Robber";
            cop = spawnVehicle(true, prefab_cop, cop_start);
            cop.name = hostVehicleName;
            robber = spawnVehicle(false, prefab_robber, robber_start);
            robber.name = clientVehicleName;
		}
		else {
			// the host is the cop
			// the client is going to be the Robber
			clientVehicleName = "Robber";
			hostVehicleName = "Cop";
            robber = spawnVehicle(true, prefab_robber, robber_start);
            robber.name = clientVehicleName;
            cop = spawnVehicle(false, prefab_cop, cop_start);
            cop.name = hostVehicleName;
		}


		return new GameObject[] {robber,cop};
	}

    public void EndLevel()
    {
        statController = GameObject.Find("StatisticsController").GetComponent<StatisticsController>();

		if (!StatisticsService.isSubmitting) statController.SubmitStatistics(); 

        displayStatistics();
        ScoreController.controller.score = 0;
       
    }

    public void doLeaveRoom() {
        MultiplayerController.Instance.leaveRoom();
    }

	public void fire_rocket (float posX, float posY, float rotZ, GameObject vehicle){ 
		GameObject rocket_prefab = (GameObject)Resources.Load ("Rocket");
		GameObject rocket = (GameObject)Instantiate (rocket_prefab, new Vector3(posX, posY, -1), Quaternion.Euler(0, 0, rotZ));
		
		
		// ensure collision doesn't happen with the car firing the bullets
		Physics2D.IgnoreCollision(rocket.GetComponent<Collider2D>(), vehicle.GetComponent<Collider2D>(), true);
	}
	// Display the in game statistics for a round - if a round has ended
    private void displayStatistics()
    {
        StatisticsPanel = GameObject.Find("Statistics");
        Animator anim = StatisticsPanel.GetComponent<Animator>();
        anim.Play("GameOver");
    }
	// Spawn a vehicle at a given location, based on being local or network, with interchangable prefab
	// ensure camera follows correct vehicle for local player
    public GameObject spawnVehicle(bool isLocal, GameObject prefab, Vector3 startPos) {
		Debug.Log("spawning vehicle...");
		GameObject vehicle = (GameObject)Instantiate (prefab, startPos, Quaternion.identity);

		if (isLocal) {
			CameraController cc = Camera.main.GetComponent<CameraController>();
			cc.target = vehicle;
			vehicle.AddComponent<LocalCarController> ().enabled = true;
			MultiplayerController.Instance.addToList (vehicle.GetComponent<LocalCarController>());
		} else {
			vehicle.AddComponent<NetworkVehicleController>().enabled = true;
			MultiplayerController.Instance.addToList (vehicle.GetComponent<NetworkVehicleController>());
		}

		return vehicle;
	}

}
