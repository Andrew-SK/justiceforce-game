﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject target;
	public float cameraLag;	// must be between 0 and 1 
	public float vertical_offset;
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (target != null) {
			Vector3 newPosition = new Vector3();

			newPosition.x = Mathf.Lerp(this.transform.position.x, target.transform.position.x, cameraLag);
			newPosition.y = Mathf.Lerp (this.transform.position.y, target.transform.position.y + vertical_offset, cameraLag);
			newPosition.z = this.transform.position.z;

			this.transform.position = newPosition;

		} else {
			target = GameObject.Find("Robber(Clone)");
		}

	}
}
