﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class TimeController : MonoBehaviour {

	float StartTime, EndTime;

	public static TimeController timeController;

	// Use this for initialization
	void Awake () {
		if (timeController == null) {
			DontDestroyOnLoad(gameObject);
			timeController = this;
		} else if (timeController != this) {
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		StartTime = Time.time;
	}

	public float FinishTime () {
		return Time.time - StartTime;
	}
}
