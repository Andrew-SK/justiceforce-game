﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Score Controller
/// </summary>
public class ScoreController : MonoBehaviour {

	// Score Controller singleton
	public static ScoreController controller;
	
	public string ScoreText;

	private static int MIN_SCORE = 0;

	public int score = 0;

	// Use this for initialization
	void Awake () {
		if (controller == null) {
			DontDestroyOnLoad(gameObject);
			controller = this;
		} else if (controller != this) {
			Destroy(gameObject);
		}
	}

	void OnGUI () {
		if (Application.loadedLevelName == "GameOver") {
			Destroy(gameObject);
			return;
		}

		UI_SetScore (score);
	}

	public void AlterScore (int score) {
		int newScore = this.score + score;

		// Ensure that the score is above the min score.
		if (newScore >= MIN_SCORE) {
			// Alter the score by the specified amoung
			this.score += score;

			// Update the GUI score text
			UI_SetScore (this.score);
		}
	}

	void UI_SetScore (int score) {
		GameObject scoreUI = GameObject.Find ("txt_ScoreValue");
		Text ScoreUIText = scoreUI.GetComponent<Text>();
		ScoreUIText.text = score.ToString();
	}
}
