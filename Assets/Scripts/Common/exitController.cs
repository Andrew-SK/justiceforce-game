﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;

public class exitController : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("entered the exit!");
		Application.LoadLevel ("GameOver");
		MultiplayerController.Instance.leaveRoom ();
	}
}
