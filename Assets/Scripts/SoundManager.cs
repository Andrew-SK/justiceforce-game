﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioSource sfxSource;
	public AudioSource musicSource;
	public static SoundManager instance = null; 
	

	public void PlaySingle (AudioClip clip) { 
		sfxSource.clip = clip;
		sfxSource.Play ();
	}

}
