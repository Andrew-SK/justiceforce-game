﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using AssemblyCSharp;

[System.Serializable]
class EventMessage : NetworkMessage
{
	public EventMessage() { }


	private float pX, pY, pZ;
	public Vector3 Position
	{
		get
		{
			return new Vector3(pX, pY, pZ);
		}

		set
		{
			pX = value.x;
			pY = value.y;
			pZ = value.z;
		}
	}

	public string entityName { get; set; }
	public string type { get; set; }
	public string otherEntities { get; set; }
}