﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using AssemblyCSharp;

[System.Serializable]
public class PhysicsMessage : NetworkMessage {

	public PhysicsMessage() { }

	public PhysicsMessage (string entityName, float health, Vector3 position, Vector3 velocity, Quaternion rotation):base(entityName,"p")
	{
		Position = position;
		Velocity = velocity;
		Rotation = rotation;
		this.health = health;
	}

	public float health { get; set; }

	private float pX;
	private float pY;
	private float pZ;
	public Vector3 Position { 
		get {
			return new Vector3 (pX, pY, pZ);
		}

		set {
			pX = value.x;
			pY = value.y;
			pZ = value.z;
		}
	}

	private float vX;
	private float vY;
	private float vZ;
	public Vector3 Velocity { 
		get {
			return new Vector3(vX, vY, vZ);
		} 
		set {
			vX = value.x;
			vY = value.y;
			vZ = value.z;
		} 
	}

	private float qW;
	private float qX;
	private float qY;
	private float qZ;
	public Quaternion Rotation { 
		get {
			return new Quaternion(qX, qY, qZ, qW);
		} 
		set {
			qW = value.w;
			qX = value.x;
			qY = value.y;
			qZ = value.z;
		} 
	}
}
