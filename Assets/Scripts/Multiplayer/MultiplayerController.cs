/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using AssemblyCSharp;
using System.IO;
using System.Text;

public class MultiplayerController : RealTimeMultiplayerListener {
	//Instance Variables of the Multiplayer Controller
	private static MultiplayerController _instance = null;
	public static Dictionary<int,NetworkUpdatableGameObject> _entityList = null;
	public static Dictionary<int,ItemController> _itemList = null;

	private GameController _gameController;

	public GameController gameController {
		get {
			if (_gameController == null) {
				_gameController = GameObject.Find ("GameController").GetComponent<GameController>();
			}
			return _gameController;
		}
	}
			
    public static Participant _host = null;
    public static Participant _client = null;
	public static string LeaderboardId = "CgkI3c-Zo4IfEAIQBg";

	//Private Variables for initiating a Google Play Services Room
	private uint minimumOpponents = 1;
	private uint maximumOpponents = 1;
	private uint gameType = 0;

	private bool _inRoom;
	public bool isInRoom {
		get
		{
			return _inRoom; 
		}
	}

	// Variables for byte array construction and sending 
	private byte _protocolVersion = 1;
	private List<byte> message;
	
	// SINGLETON METHODS

	private MultiplayerController() {

		message = new List<byte>();

		_inRoom = false;

		
	}

	// Singleton instance to adhere to delegation pattern
	public static MultiplayerController Instance {
		// from the instance build a Google Play Services platform based on the settings provided
		get {
			if (_instance == null) {
                PlayGamesPlatform.DebugLogEnabled = true;
                PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()

                    .Build();

                PlayGamesPlatform.InitializeInstance(config);
                PlayGamesPlatform.Activate();

				// Instanciate lists used for tracking/using various features
                _instance = new MultiplayerController();
				_entityList = new Dictionary<int,NetworkUpdatableGameObject>();
				_itemList = new Dictionary<int, ItemController>();
                
            }

            

			return _instance;
		}
	}

	// MATCH-MAKING METHODS

	public void SilentLogin() {
		if (!PlayGamesPlatform.Instance.localUser.authenticated) {
			PlayGamesPlatform.Instance.Authenticate((bool success)=> {
				if (success) {
					Debug.Log ("Sliently logged in "+ PlayGamesPlatform.Instance.localUser.userName);
				}
				else
				{
					Debug.Log ("Failed Silent Login");
				}
			});
		}
	}

	public byte[] sendNetworkPhysicsUpdate(int id, float health, Vector3 position, Vector3 velocity, Quaternion rotation)
	{
		message.Clear();
		message.Add(_protocolVersion);
		message.Add((byte)'P');

		// health
		message.AddRange(System.BitConverter.GetBytes(health));

		// position
		message.AddRange(System.BitConverter.GetBytes(position.x));
		message.AddRange(System.BitConverter.GetBytes(position.y));

		// velocity
		message.AddRange(System.BitConverter.GetBytes(velocity.x));
		message.AddRange(System.BitConverter.GetBytes(velocity.y));
		// rotation
		message.AddRange(System.BitConverter.GetBytes(rotation.eulerAngles.z));

		// id
		message.AddRange(System.BitConverter.GetBytes(id));
	
		byte[] toSend = message.ToArray();

//		Debug.Log("sending byte array...");	
		try
		{
			PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, toSend);
		}
		catch
		{
			Debug.Log("unable to access Google play services, assumed debugging on non-android device.");
		}
		

		return toSend;
	}

	public byte[] sendDeathMessage(int id)
	{
		message.Clear();

		message.Add(_protocolVersion);
		message.Add((byte)'D');

		// add player id 
		message.AddRange(System.BitConverter.GetBytes(id));

		byte[] toSend = message.ToArray();

		Debug.Log("Sending death message");
		try
		{
			PlayGamesPlatform.Instance.RealTime.SendMessageToAll(true, toSend);
		}
		catch
		{
			Debug.Log("unable to access Google play services, assumed debugging on non-android device.");
		}
		
		return toSend;
	}

	public byte[] sendItemPickupMessage(int item_id, int player_id) {
		message.Clear ();
		message.Add (_protocolVersion);
		message.Add ((byte)'I');

		message.AddRange (System.BitConverter.GetBytes (item_id));
		message.AddRange (System.BitConverter.GetBytes (player_id));

		byte[] toSend = message.ToArray ();

		Debug.Log ("sending item pickup message");
		return toSend;
	}

	public byte[] sendRocketFire(Vector3 pos, Quaternion direction, int id) {

		message.Clear ();

		message.Add (_protocolVersion);
		message.Add ((byte)'R');

		message.AddRange (System.BitConverter.GetBytes (pos.x));
		message.AddRange (System.BitConverter.GetBytes (pos.y));

		message.AddRange (System.BitConverter.GetBytes (direction.eulerAngles.z));

		message.AddRange (System.BitConverter.GetBytes (id));


		byte[] toSend = message.ToArray ();

		Debug.Log ("sending rocket fire message");
		try
		{
			PlayGamesPlatform.Instance.RealTime.SendMessageToAll(true, toSend);
		}
		catch
		{
			Debug.Log("unable to access Google play services, assumed debugging on non-android device.");
		}
		

		return toSend;
	}

	public void LogOut() {
		PlayGamesPlatform.Instance.SignOut ();
		Debug.Log ("Logged out");
	}
		
	public bool isLoggedIn() {
		return PlayGamesPlatform.Instance.localUser.authenticated;
	}
	
	public void StartMatchMaking() {
		if (isLoggedIn ()) {
			if(PlayGamesPlatform.Instance.RealTime.IsRoomConnected()) {
				leaveRoom();
			}
			PlayGamesPlatform.Instance.RealTime.CreateQuickGame (minimumOpponents, maximumOpponents, gameType, this);
			PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants ();
		} else {
			SilentLogin();
		}
		}
	
	public void LeaderBoard () {
		PlayGamesPlatform.Instance.ShowLeaderboardUI(LeaderboardId);
	}

	public void leaveRoom() 
	{
		PlayGamesPlatform.Instance.RealTime.LeaveRoom ();
        
	}

	// REALTIME-MULTIPLAYER INTERFACE METHODS

	private bool showingWaitingRoom = false;

	public void OnRoomSetupProgress (float percent)
	{
		Debug.Log ("Current Setup Progress Percent: " + percent.ToString());
		if (!showingWaitingRoom) {
			showingWaitingRoom = true;
			PlayGamesPlatform.Instance.RealTime.ShowWaitingRoomUI();
		}

	}

    public int hostId()
    {
        return _host.ParticipantId.GetHashCode();
    }

	public void OnRoomConnected (bool success)
	{
		_inRoom = true;
       	// if the room is connected to successfully then, from the list of participants of the room, select the host to be
		// the higher of the hash of the participant ID (an ID which is unique each time a player joins a Google Play room)
		if (success) {
			Debug.Log ("OnRoomConnect Success");
			List<Participant> participants;
			participants = PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants();

			if (participants[0].ParticipantId.GetHashCode() > participants[1].ParticipantId.GetHashCode()) {
				_host = participants[0];
                _client = participants[1];
			}
			else {
				_host = participants[1];
                _client = participants[0];
			}
            
			Debug.Log ("Host is: " + _host.ToString());
			showingWaitingRoom = false;
			Application.LoadLevel("LevelOne");
		}
		else {
			// if it fails, leave the room - most likely a Google Play bug/problem - need to try again
			Debug.Log ("OnRoomConnect Fail");
			showingWaitingRoom = false;
			PlayGamesPlatform.Instance.RealTime.LeaveRoom();
		}
	}

    

	public void OnLeftRoom ()
	{
        
        _instance = null;
		Debug.Log ("Left Room");
        
        Application.LoadLevel("Main Menu");
	}



	public void OnParticipantLeft (Participant participant)
	{
        GameObject.Find("GameController").GetComponent<GameController>().EndLevel();
        
		Debug.Log ("OnParticipantLeft: " + participant.ToString());
	}

	public void OnPeersConnected (string[] participantIds)
	{
		Debug.Log ("OnPeersConnected: " + participantIds.ToString());

	}

	public void OnPeersDisconnected (string[] participantIds)
	{
        GameObject.Find("GameController").GetComponent<GameController>().EndLevel();

          Debug.Log ("OnPeersDisconnected: " + participantIds.ToString());
	}

	public void OnRealTimeMessageReceived (bool isReliable, string senderId, byte[] data)
	{
		Debug.Log ("ORTMR isReliable: " + isReliable.ToString() + " senderId: " + senderId + " data: unimplemented" );
		receivePackage (isReliable, senderId, data);
            
    
    }

	// NETWORKING METHODS

	public string localPlayerUsername() {
		return PlayGamesPlatform.Instance.localUser.userName;
	}

	public string localPlayerId() {
		return PlayGamesPlatform.Instance.localUser.id;
	}

	public string opponentId() {
		List<Participant> participants = PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants();

		if (localPlayerId() == participants[0].ParticipantId) {
			return participants[1].ParticipantId;
		} else {
			return participants[0].ParticipantId;
		}
	}

	public bool isHost {
		get
		{
			if (_host == PlayGamesPlatform.Instance.RealTime.GetSelf())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public void addToList(NetworkUpdatableGameObject entity) {
		entity.list_id = entity.GetHashCode();
		Debug.Log("entity id is " + entity.list_id);
		_entityList.Add(entity.list_id, entity);
		Debug.Log ("The current entitList is: " + _entityList);
	}
	
	// this version of the above method is called when an object is generated based on the message recieved
	// from a network message, thus the id should be set to the one referred to in the message 
	public void addToList(NetworkUpdatableGameObject entity, int id)
	{
		entity.list_id = id;
		_entityList.Add(entity.list_id, entity);
		Debug.Log("The current entitList is: " + _entityList);
	}

	public void addToItemList(ItemController itemController) {
		Debug.Log ("Attempting to add item to the list");
		itemController.list_id = _itemList.Count;
		_itemList.Add (itemController.list_id, itemController);
	}

	public void addToList(ItemController itemController, int id) {
		itemController.list_id = id;
		_itemList.Add (itemController.list_id, itemController);
	}


	public void deleteFromList(string id) {
		throw new System.NotImplementedException ();
	}

	// Based on the type of message received 

	public void receivePackage(bool isReliable, string senderId, byte[] data){

		int entity_id;
		float posX, posY, rotZ;

		byte version = (byte)data[0];
		char messageType = (char)data[1];

		Debug.Log("recieved message type: " + messageType + " length: " + data.Length);

		switch(messageType)
		{
			case ('P'): // Physics update
				
				float health = System.BitConverter.ToSingle(data, 2);
				posX = System.BitConverter.ToSingle(data, 6);
				posY = System.BitConverter.ToSingle(data, 10);
                float velX = System.BitConverter.ToSingle(data, 14);
                float velY = System.BitConverter.ToSingle(data, 18);
				rotZ = System.BitConverter.ToSingle(data, 22);
				entity_id = System.BitConverter.ToInt32(data, 26);
				
				_entityList[entity_id].receivePhysicsUpdate(health, posX, posY,velX,velY,rotZ);
				break;

			case ('D'): // Death message

				entity_id = System.BitConverter.ToInt32(data, 2);

				if (_entityList.ContainsKey(entity_id))
					((NetworkVehicleController)_entityList[entity_id]).healthController.SetHealth(0); // this kills the player
	
				break;

			case ('R'): // Rocket fire message

				posX = System.BitConverter.ToSingle(data, 2);
				posY = System.BitConverter.ToSingle(data, 6);
				rotZ = System.BitConverter.ToSingle(data, 10);
				entity_id = System.BitConverter.ToInt32(data, 14);
				gameController.fire_rocket(posX, posY, rotZ, _entityList[entity_id].gameObject);
			
				break;
		}
	}

}
