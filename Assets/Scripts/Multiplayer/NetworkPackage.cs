/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using System.Collections;
using System.Collections.Generic;
using MsgPack;
using AssemblyCSharp;

[System.Serializable]
public class NetworkPackage {

	public List<NetworkMessage> Messages { get; set; }

	public NetworkPackage() {
		Messages = new List<NetworkMessage>();
	}

	public void addMessage(NetworkMessage currentMsg) {
		Messages.Add (currentMsg);
	}
	public void PackToMessage (Packer packer, PackingOptions options)
	{
		throw new System.NotImplementedException ();
	}

	public void UnpackFromMessage (Unpacker unpacker)
	{
		throw new System.NotImplementedException ();
	}
}
