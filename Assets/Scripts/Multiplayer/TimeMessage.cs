﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System;
using System.Collections;
using AssemblyCSharp;

[System.Serializable]
public class TimeMessage : NetworkMessage {
	public DateTime time { get; set; }
	public string type { get; set; }

	public TimeMessage() {}

	public TimeMessage(string entityName, DateTime time, string type) : base (entityName, "time")  {
		this.time = time;
		this.type = type;
	}
}
