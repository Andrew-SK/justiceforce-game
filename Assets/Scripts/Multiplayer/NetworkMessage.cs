﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using AssemblyCSharp;

[System.Serializable]
public class NetworkMessage {

	public string EntityName { get; set; }
	public string MessageType { get; set; }


	public NetworkMessage() {}

	public NetworkMessage (string entityId, string messageType)
	{
		this.EntityName = entityId;
		this.MessageType = messageType;
	}
}
