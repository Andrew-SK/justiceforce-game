﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using AssemblyCSharp;

[System.Serializable]
public class HealthMessage : NetworkMessage {
	public float health { get; set; }

	public HealthMessage () {}

	public HealthMessage (string entityName, float health) : base (entityName, "health") {
		this.health = health;
	}
}
