﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MatchMakingController : MonoBehaviour {

	public void ToAutoMatch () {
		MultiplayerController.Instance.StartMatchMaking();
	}
}
