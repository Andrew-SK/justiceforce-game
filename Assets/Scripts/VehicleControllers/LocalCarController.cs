﻿/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using AssemblyCSharp;

public class LocalCarController : NetworkUpdatableGameObject {

	public Vector3 target;
	public float maxTurningArch = 5; // Degrees
	public float speed = 1700;
	public HealthController healthController;
    public ScoreController scoreController;
	public StatisticsController statisticsController;
	public bool inventoryFull = false; // inventory full boolean
	public bool candestroy = false;
	public bool check = true;
	public Sprite[] items;
	public GameObject Inventory;
	public GameObject enemy;
	public GameObject enemy2;
	public Button InventoryButton;
	public Image InventoryRenderer;
	public GameController gameController;
	public GameObject rocket_prefab;
	public bool allowFire = true;

	private static float COLLISION_DAMAGE = -3.0f;
	private static float POTION_HEALTH = 50.0f;
	private static float DAMAGE = -10.0f;
	private static float DISTANCE = 10;
	private static float NEW_BOOST_SPEED = 2200;
	private static float ROCKET_BOOST = 300;

	private static int SCORE_SCALAR = 20;

	private Rigidbody2D rb;
	private float nextBroadcastTime = 0f;
	private float targetDistanceThreshold = 1.0f;
	private bool itemPicked = false;
	private string itemPickedName;

	// Use this for initialization
	void Start () {
		gameController = GameObject.Find ("GameController").GetComponent<GameController>();
		rocket_prefab = (GameObject)Resources.Load("Rocket");
	}

	// Instalisation 
	void Awake() {
		rb = GetComponent<Rigidbody2D> ();
		healthController = gameObject.GetComponent<HealthController>();
		scoreController = GameObject.Find ("ctrl_Score").GetComponent<ScoreController>();
		statisticsController = GameObject.Find ("StatisticsController").GetComponent<StatisticsController>();
		Inventory = GameObject.Find ("Canvas/item_Btn");
		InventoryButton = Inventory.GetComponent<Button> ();
		InventoryRenderer = Inventory.GetComponent<Image> ();
		Inventory.SetActive(false);
		

		InventoryButton.onClick.AddListener (() => ClickItem());
		// If player is host, send start time message
//		if (MultiplayerController.Instance.isHost()) {
//			string time = DateTime.Now.ToString();
//			MultiplayerController.Instance.addTimeMessage (gameObject.name, time, "start");
//			// TODO: Add start time to statistics (once feature/game_statistics is merged)
//		}
	}

	// Update is called once per frame
	void Update()
	{
		// If left (0) mouse button currently held down
		if (Input.GetMouseButton(0) || Input.touchCount > 0)
		{
			// find the location of the touch or mouse pointer in world coordinates
			if (Input.touchCount > 0)
			{
				target = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			}
			else
			{
				target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			}
			// If there is a second touch or mouse click on the screen
			if (Input.touchCount > 1 || Input.GetMouseButton(1))
			{
				// Shoot a rocket!
				if (allowFire == true && gameObject.name == "Cop")
				{
					allowFire = false;
					Fire();
					// Coroutine only lets the player shoot a rocket every 2 secs
					StartCoroutine(Waittofire((test) =>
					{
						this.allowFire = true;
					}));
				}
			}
			target.z = -1;
			driveTowards(target);
		}
		if (Time.time > nextBroadcastTime)
		{
			MultiplayerController.Instance.sendNetworkPhysicsUpdate(list_id, healthController.GetHealth(), transform.position, rb.velocity, transform.rotation);
			nextBroadcastTime = Time.time + NetworkVehicleController.updatePeriod;
		}
	}

	public void ClickItem() {
		if (itemPicked == true) { 
			// Checks which item was picked up, and calls use function accordingly
			if (itemPickedName == "Bomb") UseBomb();
			if (itemPickedName == "HealthPotion") UseHealth();
			if (itemPickedName == "SpeedBoost") UseBoost();
		}
	}


	void OnCollisionEnter2D(Collision2D other) {
		if (healthController.GetHealth () > 0) {
			// If hit by rocket, be damaged accordingly
			if (other.gameObject.name.Contains ("Rocket")) {
				healthController.AlterUIHealth (DAMAGE);
			} else {
				healthController.AlterUIHealth (COLLISION_DAMAGE * rb.velocity.magnitude);
			}
			healthController.ShowHealthBar ();
		} 
		// If a players health is <= 0, then it's game over!
		if (healthController.GetHealth () <= 0) {
			MultiplayerController.Instance.sendDeathMessage(list_id);
            GameObject.Find("GameController").GetComponent<GameController>().EndLevel();
        }
		// If two players collide, they are damanged acorrding to velocity 
        if (other.gameObject.name.Contains("Cop") || other.gameObject.name.Contains("Robber")) {
			int score = (int)rb.velocity.magnitude * SCORE_SCALAR;
			scoreController.AlterScore(score);
			statisticsController.currentRound.score += score;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		handleItemCollision(other.gameObject.GetComponent<ItemController>());
	}

	private void handleItemCollision(ItemController itemController)
	{
		// Picks up an item, if inventory isn't full
		if (!inventoryFull)	{
			if (itemController.name == "Bomb")	{
				int num = 0;
				itemPickedName = itemController.name;
				CollectItem(itemController, num);
			}
			else if (itemController.name == "HealthPotion") {
				int num = 1;
				itemPickedName = itemController.name;
				CollectItem(itemController, num);
			}
			else if (itemController.name == "SpeedBoost") {
				int num = 2;
				itemPickedName = itemController.name;
				CollectItem(itemController, num);
			}
		}
	}

	void OnGUI() {
		if (!inventoryFull) { // double check inventory isn't full as we don't want to render > 1 item
			if (itemPicked) { 
				inventoryFull = true;
			}
		}
	}

	// Drive towards the touch input
	void driveTowards(Vector3 target) {
		Vector3 path = target - transform.position;
		float distance = Vector3.Distance (target, transform.position);

		if (distance > targetDistanceThreshold) {
			transform.up = Vector3.Slerp (transform.up, Vector3.Normalize (path), maxTurningArch * Time.deltaTime);

			rb.AddForce(transform.up * speed * Time.deltaTime);
		}
	}

	// Gets the collision with item gameobject
	bool isCollidingWithItem(Collision2D other) {
		return (other.gameObject.GetComponent<ItemController>());
	}

	// Interface methods

	public override void receivePhysicsUpdate (float health, float posX, float posY, float velX, float velY, float rotZ)
	{
		throw new System.NotImplementedException ();
	}

	public override void sendNetworkPhysicsUpdate(string id, float health, Vector3 position, Vector3 velocity, Quaternion rotation) 
	{
		// unimplemented
	}

	public override int GetHashCode()
	{
		return gameObject.name.GetHashCode();
	}

	// Rocket fire function
	void Fire() {
		Vector3 pos;
		// Get the second touch/click position for aiming
        if (Input.touchCount > 1)
		{
			pos = Input.GetTouch(1).position;
		} else
		{
			pos = Input.mousePosition;
		}
		// Set up rocket aim at mouse click, gives some assist
		pos.z = transform.position.z - Camera.main.transform.position.z;
		pos = Camera.main.ScreenToWorldPoint(pos);
		Quaternion aim = Quaternion.FromToRotation(Vector3.up, pos - transform.position);
		// Instantiate a single rocket
		Vector3 world_pos = transform.position + transform.up;
		GameObject rocket = (GameObject)Instantiate (rocket_prefab, world_pos, aim);

		
		// ensure collision doesn't happen with the car firing the bullets
		Physics2D.IgnoreCollision(rocket.GetComponent<Collider2D>(), GetComponent<Collider2D>());
		// Multiplayer fire!
		MultiplayerController.Instance.sendRocketFire (world_pos, aim, list_id);
	}

	// Coroutine to hold off fire for 2 secs
	IEnumerator Waittofire(Action<bool> cb) {
		yield return new WaitForSeconds (2);
		cb (true);
	}
	// Updates the cars speed, after the speed boost item is used
	void UseBoost() {
		speed = NEW_BOOST_SPEED;
		StartCoroutine (speedCounter ());
		ResetInventory ();
	}

	// Coroutine for a 5 second speed boost
	IEnumerator speedCounter() {
		yield return new WaitForSeconds (5);
		revertSpeed ();
	}

	// Speed boost complete, revert to original speed
	void revertSpeed() {
		speed = 1700;
	}

	// Use health item function; player health increase
	void UseHealth() {
		if (healthController.GetHealth () > 0) {
			healthController.AlterUIHealth (POTION_HEALTH);
			ResetInventory ();
		} else {
			GameObject.Find("GameController").GetComponent<GameController>().EndLevel();
		}
	}

	// Use bomb item function; player is dealt damage
	void UseBomb() {
		if (healthController.GetHealth () > 0) {
			healthController.AlterUIHealth (DAMAGE);
			ResetInventory();
		} else {
			GameObject.Find("GameController").GetComponent<GameController>().EndLevel();
		}
	}

	// When an item is used, clear the inventory so they can
	// pick up another one
	void ResetInventory() {
		Inventory.SetActive(false);
		itemPicked = false;
		inventoryFull = false;
	}

	// Pick up an item function!
	void CollectItem(ItemController other, int num) {
		itemPicked = true;
		Inventory.SetActive (true);
		InventoryRenderer.sprite = gameController.items [num];
		Destroy (other.gameObject);
	}
}
