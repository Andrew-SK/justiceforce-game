/**
 * © Justice Force 2015 (Daniel Schulz, Emma Jamieson, Andrew Kemm, Jake Moxey)
 */
using System;
using UnityEngine;
using AssemblyCSharp;


public class NetworkVehicleController : NetworkUpdatableGameObject
{
	public HealthController healthController;

	public Rigidbody2D rb;

    private Vector3 startPosition;
    private Vector3 endPosition;
    private Quaternion startRotation;
    private Quaternion endRotation;
    private float updateTime;
    public static float updatePeriod = 0.1f;
    private Vector3 lastVelocity;


    public NetworkVehicleController ()
	{
	}
	
	public void Awake() {
		healthController = gameObject.GetComponent<HealthController>();
	}

	public void Start() {
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

	public void Update() {
        float percentageTime = (Time.time - updateTime) / updatePeriod;

        if (percentageTime <= 1.0)
        {
            transform.position = Vector3.Lerp(startPosition, endPosition, percentageTime);
            transform.rotation = Quaternion.Slerp(startRotation, endRotation, percentageTime);
        }
        else {
            transform.position = transform.position + (lastVelocity * Time.deltaTime);
        }
    }

	public override void receivePhysicsUpdate(float health, float posX, float posY,float velX,float velY, float rotZ) {

        startPosition = transform.position;
		endPosition = new Vector3(posX, posY, transform.position.z);

        startRotation = transform.rotation;
		endRotation = Quaternion.Euler(0, 0, rotZ);

        updateTime = Time.time;
        lastVelocity = new Vector3(velX, velY, 0);
        //lastVelocity = new Vector3()
        //gameObject.transform.position = phys_msg.Position;
        //gameObject.GetComponent<Rigidbody2D>().velocity = phys_msg.Velocity;
        //gameObject.transform.rotation = phys_msg.Rotation;
        receiveHealth(health);

	}

	public void receiveHealth (float health) {
		Debug.Log("network vehicle health set to " + health);
		if (health > 0) {
			healthController.SetHealth(health);
			healthController.ShowHealthBar ();
		} 
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		Destroy (other.gameObject);
	}
	
	public override void sendNetworkPhysicsUpdate(string id, float health, Vector3 position, Vector3 velocity, Quaternion rotation) { }

	public override int GetHashCode()
	{
		return gameObject.name.GetHashCode();
	}
}

