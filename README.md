# Justice Force

## About

JusticeForce is a two dimensional, top down cops and robbers smart-phone game. It can be played in either single or multi-player mode. 
In multi-player mode, two players begin at the starting tile, one a cop, one a robber. The cops aim is to defeat the robber before reaches the end tile of the second level. The robbers aim is to either make it to the end of the two levels or defeat the cop before hand. Players can utilise items picked up during game play to gain health, a speed boost or bomb the opposing player; they can further shoot a rocket at the other player every two seconds by touching with a second finger. 

The game was developed over three iterations, using C#, Unity and Google Play Services. 


## Requirements

* Unity 5
* An android device (for multiplayer capabilities) with Google Play Account


## How to compile and run on android

1. Connect your android device to your computer
2. In Unity, go to File -> Build Settings... -> Player Settings...
3. Set the **Keystore password** and Key **Password** to `ilgampff11`
4. In Build Settings, click **Build And Run** 
5. Wait until the app runs on your device :)


## Web App

To search through players statistics check out -

www.justiceforce-game.net

# Testing Modules

The unit tests for the game scripts are located in the `Assets/Editor/ScriptsTests/` folder, and can be run by:

1. Clicking on 'Unity Test Tools -> Unit Test Runner' in the toolbar.
2. Click on 'JusticeForceTests' in the window that pops up.
3. Click on 'Run Selected' in the window toolbar.
4. Then the result will be shown on the same window.


# Developers

Daniel Schulz, Andrew Kemm, Jake Moxey and Emma Jamieson.